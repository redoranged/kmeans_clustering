  --
  -- Database: `kmeans_clustering`
  --

  DROP DATABASE IF EXISTS kmeans_clustering;

  CREATE DATABASE kmeans_clustering;
  USE kmeans_clustering;

  -- --------------------------------------------------------

  --
  -- Table structure for table `users`
  --
  DROP TABLE IF EXISTS `users`;
  CREATE TABLE `users` (
    `user_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `role` enum("admin", "user_1", "user_2") NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`user_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  --  INSERT Default User 
  --  Username : admin
  --  Password : admin
  --

  INSERT INTO `users` SET `name`='Admin', `username`='admin', `password`='21232f297a57a5a743894a0e4a801fc3', `role`='admin';

  --
  -- Table structure for table `data_produksis`
  --
  DROP TABLE IF EXISTS `data_produksis`;
  CREATE TABLE `data_produksis` (
    `data_produksi_id` INT NOT NULL AUTO_INCREMENT,
    `kecamatan` VARCHAR(255) NOT NULL,
    `bawang_daun` FLOAT DEFAULT 0,
    `bawang_merah` FLOAT DEFAULT 0,
    `bayam` FLOAT DEFAULT 0,
    `kacang_panjang` FLOAT DEFAULT 0,
    `kembang_kol` FLOAT DEFAULT 0,
    `labu_siam` FLOAT DEFAULT 0,
    `sawi` FLOAT DEFAULT 0,
    `cabai_besar` FLOAT DEFAULT 0,
    `cabai_rawit` FLOAT DEFAULT 0,
    `ketimun` FLOAT DEFAULT 0,
    `tomat` FLOAT DEFAULT 0,
    `terung` FLOAT DEFAULT 0,
    `kangkung` FLOAT DEFAULT 0,
    `average` FLOAT DEFAULT 0,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`data_produksi_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `centeroid_produksis`
  --
  DROP TABLE IF EXISTS `centeroid_produksis`;
  CREATE TABLE `centeroid_produksis` (
    `centeroid_produksi_id` INT NOT NULL AUTO_INCREMENT,
    `cluster` CHAR(5) NOT NULL,
    `bawang_daun` FLOAT DEFAULT 0,
    `bawang_merah` FLOAT DEFAULT 0,
    `bayam` FLOAT DEFAULT 0,
    `kacang_panjang` FLOAT DEFAULT 0,
    `kembang_kol` FLOAT DEFAULT 0,
    `labu_siam` FLOAT DEFAULT 0,
    `sawi` FLOAT DEFAULT 0,
    `cabai_besar` FLOAT DEFAULT 0,
    `cabai_rawit` FLOAT DEFAULT 0,
    `ketimun` FLOAT DEFAULT 0,
    `tomat` FLOAT DEFAULT 0,
    `terung` FLOAT DEFAULT 0,
    `kangkung` FLOAT DEFAULT 0,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`centeroid_produksi_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `product`
  --
  DROP TABLE IF EXISTS `product`;
  CREATE TABLE `product` (
    `product_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`product_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `customer`
  --
  DROP TABLE IF EXISTS `customer`;
  CREATE TABLE `customer` (
    `customer_id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`customer_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `data_pelanggans`
  --
  DROP TABLE IF EXISTS `data_pelanggans`;
  CREATE TABLE `data_pelanggans` (
    `data_pelanggan_id` INT NOT NULL AUTO_INCREMENT,
    `customer_id` INT NOT NULL,
    -- `customer_id` VARCHAR(255) NOT NULL,
    `invoice_no` VARCHAR(255) NOT NULL,
    `tanggal_transaksi` DATE NOT NULL,
    `product_id` INT NOT NULL,
    -- `product_id` VARCHAR(255) NOT NULL,
    `jumlah` FLOAT DEFAULT 0,
    `harga_satuan` FLOAT DEFAULT 0,
    `total_bayar` FLOAT DEFAULT 0,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`data_pelanggan_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Table structure for table `centeroid_pelanggans`
  --
  DROP TABLE IF EXISTS `centeroid_pelanggans`;
  CREATE TABLE `centeroid_pelanggans` (
    `centeroid_pelanggan_id` INT NOT NULL AUTO_INCREMENT,
    `cluster` CHAR(5) NOT NULL,
    `recency` FLOAT DEFAULT 0,
    `frequency` FLOAT DEFAULT 0,
    `monetary` FLOAT DEFAULT 0,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (`centeroid_pelanggan_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;