<?php
require_once 'Model.php';

class Customer extends Model
{
    public $name = 'Customer';
    public $table = 'customer';
    public $primaryKey = 'customer_id';
    protected $columns = ['name'];
}
?>