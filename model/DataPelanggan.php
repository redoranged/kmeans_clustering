<?php
require_once 'Model.php';

class DataPelanggan extends Model
{
    public $name = 'Data Pelanggan';
    public $table = 'data_pelanggans';
    public $primaryKey = 'data_pelanggan_id';
    protected $columns = ['customer_id', 'invoice_no', 'tanggal_transaksi', 'product_id', 'jumlah', 'harga_satuan', 'total_bayar']; 

    function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->initialRelation();
    }

    function initialRelation()
    {
        $this->many2one[] = [
            'class' => new Customer(),
            'fkey' => 'customer_id',
            'pkey' => 'customer_id'
        ];
        $this->many2one[] = [
            'class' => new Product(),
            'fkey' => 'product_id',
            'pkey' => 'product_id'
        ];
    }

    /**
     * RFM
     */

    public function selectRFM($n = true, $average = null)
    {
        $customer = new Customer();
        $customers = $customer->select();
        $res = [];
        foreach($customers as $row)
        {
            $data = $this->select("WHERE customer_id=".$row['customer_id']);
            if(count($data) > 0)
            {
                $recency = $this->getRecency($row['customer_id'], $n);
                $frequency = $this->getFrequency($row['customer_id'], $n);
                $monetary = $this->getMonetary($row['customer_id'], $n);
                $avg = number_format(($recency+$frequency+$monetary)/3, 2);
                $res[] = [
                    'pelanggan' => $row,
                    'recency' => $recency,
                    'frequency' => $frequency,
                    'monetary' => $monetary,
                    'average' => $avg
                ];

                if(!is_null($average))
                {
                    if($average == $avg)
                    {
                        return [
                            [
                                'pelanggan' => $row,
                                'recency' => $recency,
                                'frequency' => $frequency,
                                'monetary' => $monetary,
                                'average' => $avg
                            ]
                        ];
                    }
                }
            }
        }

        return $res;
    }

    function getRecency($customer_id, $n = true)
    {
        $query = "SELECT DATEDIFF(NOW(), MAX(`tanggal_transaksi`)) AS `recency` FROM ".$this->table." WHERE customer_id=".$customer_id;

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $recency = [];
        while ($row=$result->fetch_assoc())
        {
            $recency = $row['recency'];
        }

        if(!$n)
        {
            return $recency;
        }

        $query = "SELECT DATEDIFF(NOW(), `tanggal_transaksi`) AS `recency` FROM ".$this->table;

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $arrayRecency = [];
        while ($row=$result->fetch_assoc())
        {
            $arrayRecency[] = $row['recency'];
        }

        return normalisasi($recency, $arrayRecency);
    }

    function getFrequency($customer_id, $n = true)
    {
        $query = "SELECT COUNT(DISTINCT(`invoice_no`)) AS `frequency` FROM ".$this->table." WHERE customer_id=".$customer_id;

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $frequency = [];
        while ($row=$result->fetch_assoc())
        {
            $frequency = $row['frequency'];
        }
        
        if(!$n)
        {
            return $frequency;
        }

        $query = "SELECT COUNT(DISTINCT(`invoice_no`)) AS `frequency` FROM ".$this->table." GROUP BY `customer_id`";

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $arrayFrequency = [];
        while ($row=$result->fetch_assoc())
        {
            $arrayFrequency[] = $row['frequency'];
        }


        return normalisasi($frequency, $arrayFrequency);
    }

    function getMonetary($customer_id, $n = true)
    {
        $query = "SELECT SUM(`total_bayar`) AS `monetary` FROM ".$this->table." WHERE customer_id=".$customer_id;

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $monetary = [];
        while ($row=$result->fetch_assoc())
        {
            $monetary = $row['monetary'];
        }
        
        if(!$n)
        {
            return $monetary;
        }

        $query = "SELECT SUM(`total_bayar`) AS `monetary` FROM ".$this->table." GROUP BY customer_id";

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $arrayMonetary = [];
        while ($row=$result->fetch_assoc())
        {
            $arrayMonetary[] = $row['monetary'];
        }

        return normalisasi($monetary, $arrayMonetary);
    }

    /**
     * K-Means
     */

    public function getFirstCenteroid()
    {
        $datas = $this->selectRFM();

        $averages = array_column($datas, 'average');

        $min = min($averages);
        $max = max($averages);
        if(count(array_unique($averages)) >= 4)
        {
            $filtered = array_values(array_filter(array_unique($averages), function($value) use ($min, $max){
                return ($value != $min && $value != $max);
            }));
            $x = $filtered[rand(0,count($filtered)-1)];
        }else{
            $x = $averages[rand(0,count($averages)-1)];
        }
        $mid1 = $x;

        if(count(array_unique($averages)) >= 4)
        {
            $filtered = array_values(array_filter(array_unique($averages), function($value) use ($min, $max, $mid1){
                return ($value != $min && $value != $max && $value != $mid1);
            }));
            $x = $filtered[rand(0,count($filtered)-1)];
        }else{
            $x = $averages[rand(0,count($averages)-1)];
        }
        $mid2 = $x;
        
        $max = $this->selectRFM(true, $max)[0];
        $res[] = [
            $max['recency'],
            $max['frequency'],
            $max['monetary'],
        ];
        
        $mid1 = $this->selectRFM(true, $mid1)[0];
        $res[] = [
            $mid1['recency'],
            $mid1['frequency'],
            $mid1['monetary'],
        ];

        $mid2 = $this->selectRFM(true, $mid2)[0];
        $res[] = [
            $mid2['recency'],
            $mid2['frequency'],
            $mid2['monetary'],
        ];

        $min = $this->selectRFM(true, $min)[0];
        $res[] = [
            $min['recency'],
            $min['frequency'],
            $min['monetary'],
        ];

        // echo json_encode($res);
        // die();

        $centeroidPelanggan = new CenteroidPelanggan();
        $centeroidPelanggan->setCenteroid($res);
        
        return $res;
    }

    public function getEuclidian($centeroid)
    {
        $datas = $this->selectRFM();

        $res = [];

        foreach($datas as $data)
        {
            $a = [
                $data['recency'],
                $data['frequency'],
                $data['monetary'],
            ];
            $c1 = eucDistance($centeroid[0], $a);
            $c2 = eucDistance($centeroid[1], $a);
            $c3 = eucDistance($centeroid[2], $a);
            $c4 = eucDistance($centeroid[3], $a);
            
            $min = min([$c1, $c2, $c3, $c4]);
            $cluster = '';
            if($min == $c1)
            {
                $cluster = 'C1';
            }
            if($min == $c2)
            {
                $cluster = 'C2';
            }
            if($min == $c3)
            {
                $cluster = 'C3';
            }
            if($min == $c4)
            {
                $cluster = 'C4';
            }
            $res[] = [
                'pelanggan' => $data['pelanggan'],
                'recency' => $data['recency'],
                'frequency' => $data['frequency'],
                'monetary' => $data['monetary'],
                'C1' => $c1,
                'C2' => $c2,
                'C3' => $c3,
                'C4' => $c4,
                'cluster' => $cluster
            ];
        }

        return $res;
    }

    public function getNewCenteroid($euc)
    {
        $c1 = [];
        $c2 = [];
        $c3 = [];
        $c4 = [];
        foreach ($euc as $row) {
            if($row['cluster'] === 'C1')
            {
                $c1[] = $row;
            }
            if($row['cluster'] === 'C2')
            {
                $c2[] = $row;
            }
            if($row['cluster'] === 'C3')
            {
                $c3[] = $row;
            }
            if($row['cluster'] === 'C4')
            {
                $c4[] = $row;
            }
        }
        
        // C1
        $c1 = [
            (count($c1) > 0) ? array_sum(array_column($c1, 'recency'))/count($c1) : 0,
            (count($c1) > 0) ? array_sum(array_column($c1, 'frequency'))/count($c1) : 0,
            (count($c1) > 0) ? array_sum(array_column($c1, 'monetary'))/count($c1) : 0,
        ];
        // C2
        $c2 = [
            (count($c2) > 0) ? array_sum(array_column($c2, 'recency'))/count($c2) : 0,
            (count($c2) > 0) ? array_sum(array_column($c2, 'frequency'))/count($c2) : 0,
            (count($c2) > 0) ? array_sum(array_column($c2, 'monetary'))/count($c2) : 0,
        ];
        // C3
        $c3 = [
            (count($c3) > 0) ? array_sum(array_column($c3, 'recency'))/count($c3) : 0,
            (count($c3) > 0) ? array_sum(array_column($c3, 'frequency'))/count($c3) : 0,
            (count($c3) > 0) ? array_sum(array_column($c3, 'monetary'))/count($c3) : 0,
        ];
        // C4
        $c4 = [
            (count($c4) > 0) ? array_sum(array_column($c4, 'recency'))/count($c4) : 0,
            (count($c4) > 0) ? array_sum(array_column($c4, 'frequency'))/count($c4) : 0,
            (count($c4) > 0) ? array_sum(array_column($c4, 'monetary'))/count($c4) : 0,
        ];
        return [$c1,$c2,$c3,$c4];
    }

    // Import

    public function checkImportFormat($column_name, $columns, $columns_check_alternative = [])
    {
        if (($key = array_search('customer_id', $columns)) !== false) {
            unset($columns[$key]);
        }
        if (($key = array_search('product_id', $columns)) !== false) {
            unset($columns[$key]);
        }
        if (($key = array_search('total_bayar', $columns)) !== false) {
            unset($columns[$key]);
        }
        $columns = array_values($columns);

        array_push($columns, 'pelanggan', 'produk');

        $new_format = ['CustomerID', 'InvoiceNo', 'InvoiceDate', 'Description', 'Quantity', 'UnitPrice', 'amount/total'];

        return parent::checkImportFormat($column_name, $columns, $new_format);
    }

    /**
     * Basic Function
     */

    function additionalField($array)
    {
        if(array_key_exists('pelanggan', $array))
        {
            $customer = new Customer();
            $customer_id = $customer->findOrCreate(['name' => $array['pelanggan']]);
            $array += ['customer_id' => $customer_id['customer_id']];
            unset($array['pelanggan']);
        }
        
        if(array_key_exists('produk', $array))
        {
            $product = new Product();
            $product_id = $product->findOrCreate(['name' => $array['produk']]);
            $array += ['product_id' => $product_id['product_id']];
            unset($array['produk']);
        }

        /**
         * New Format
         */

        if(array_key_exists('CustomerID', $array))
        {
            $customer = new Customer();
            $customer_id = $customer->findOrCreate(['name' => $array['CustomerID']]);
            $array += ['customer_id' => $customer_id['customer_id']];
            unset($array['CustomerID']);
        }

        if(array_key_exists('InvoiceNo', $array))
        {
            $array += ['invoice_no' => $array['InvoiceNo']];
            unset($array['InvoiceNo']);
        }

        if(array_key_exists('InvoiceDate', $array))
        {
            $array += ['tanggal_transaksi' => $array['InvoiceDate']];
            unset($array['InvoiceDate']);
        }

        if(array_key_exists('Description', $array))
        {
            $product = new Product();
            $product_id = $product->findOrCreate(['name' => $array['Description']]);
            $array += ['product_id' => $product_id['product_id']];
            unset($array['Description']);
        }

        if(array_key_exists('Quantity', $array))
        {
            $array += ['jumlah' => $array['Quantity']];
            unset($array['Quantity']);
        }

        if(array_key_exists('UnitPrice', $array))
        {
            $array += ['harga_satuan' => $array['UnitPrice']];
            unset($array['UnitPrice']);
        }

        if(array_key_exists('amount/total', $array))
        {
            $array += ['total_bayar' => $array['amount/total']];
            unset($array['amount/total']);
        }

        if(!array_key_exists('total_bayar', $array))
        {
            $array += ['total_bayar' => $array['jumlah'] * $array['harga_satuan']];
        }

        return $array;
    }

    public function findOrCreate($array)
    {
        $array = $this->additionalField($array);
        
        return parent::findOrCreate($array);
    }

    public function createOrUpdate($array)
    {
        $array = $this->additionalField($array);
        
        return parent::createOrUpdate($array);
    }

    public function create($array)
    {
        $array = $this->additionalField($array);

        return parent::create($array);
    }

    public function update($id, $array)
    {
        $array = $this->additionalField($array);

        return parent::update($id, $array);
    }

    public function truncate()
    {
        $model = new CenteroidPelanggan();
        if(!$model->truncate())
        {
            return false;
        }
        return parent::truncate();
    }

    public function setKeyValue($key, $value)
    {
        if($key == 'tanggal_transaksi')
        {  
            $dates = explode("/", $value);
            $date = $dates[1].'-'.$dates[0].'-'.$dates[2];
            $value = date('Y-m-d', strtotime($date));
        }

        if($key == 'InvoiceDate')
        {  
            $dates = explode("/", $value);
            $date = $dates[1].'-'.$dates[0].'-'.$dates[2];
            $value = date('Y-m-d', strtotime($date));
        }
        return parent::setKeyValue($key, $value);
    }
}
?>