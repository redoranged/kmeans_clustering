<?php
/**
 * Composer Library
 */

require_once 'vendor/autoload.php';

/**
 * Initialize
 */

require_once 'config.php';

session_start();

define('URL', $protocol.'://'.$url);
define('ROOT', $root_path.$app_name);

define('DB_HOST', $db_host);
define('DB_USER', $db_user);
define('DB_PASS', $db_pass);
define('DB_NAME', $db_name);

require_once 'library/DB.php';
require_once 'library/Session.php';
require_once 'library/function.php';
require_once 'library/Auth.php';
require_once 'library/rfm.php';

$session = new Session();
$auth = new Auth();

$sayur = [
    'bawang_daun' => 'bawang_daun', 
    'bawang_merah' => 'bawang_merah', 
    'bayam' => 'bayam', 
    'kacang_panjang' => 'kacang_panjang', 
    'kembang_kol' => 'kembang_kol',
    'labu_siam' => 'labu_siam', 
    'sawi' => 'sawi', 
    'cabai_besar' => 'cabai_besar', 
    'cabai_rawit' => 'cabai_rawit', 
    'ketimun' => 'ketimun', 
    'tomat' => 'tomat', 
    'terung' => 'terung', 
    'kangkung' => 'kangkung'
];
define('SAYUR', $sayur);

/**
 * Importing Models
 */

require_once 'model/User.php';
require_once 'model/DataProduksi.php';
require_once 'model/CenteroidProduksi.php';
require_once 'model/Product.php';
require_once 'model/Customer.php';
require_once 'model/CenteroidPelanggan.php';
require_once 'model/DataPelanggan.php';
?>