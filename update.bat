@ECHO OFF
echo =======================
echo Updating Repository
echo =======================
echo.
git checkout . && git pull
SLEEP 1
echo.
echo =======================
echo Composer Update
echo =======================
echo.
call composer update
SLEEP 1
echo.
echo =======================
echo Updating Database
echo =======================
echo.
php migrate.php
SLEEP 1
echo Success Import Database
echo.
echo =======================
echo Done
echo =======================
SLEEP 3