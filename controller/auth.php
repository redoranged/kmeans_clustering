<?php
if(isset($_GET['login']))
{
    $request = getRequest();
    if(!isset($request['username']) || !isset($request['password']))
    {
        echo "<script>window.location.replace('".url('/login')."')</script>";
        exit;
    }
    $user = new User();
    $user = $user->login($request['username'], $request['password']);

    if(!empty($user))
    {
        $auth->setUser($user['user_id'], $user['name'], $user['username'], $user['role']);
        echo "<script>window.location.replace('".url('/')."')</script>";
        exit;
    }else{
        echo "<script>window.location.replace('".url('/login')."')</script>";
        exit;
    }
}

if(isset($_GET['register']))
{
    $request = getRequest();
    if(!isset($request['name']) || !isset($request['username']) || !isset($request['password']))
    {
        echo "<script>window.location.replace('".url('/register')."')</script>";
        exit;
    }
    $user = new User();
    $user = $user->create($request);

    if(!empty($user))
    {
        $auth->setUser($user['user_id'], $user['name'], $user['username'], $user['role']);
        echo "<script>window.location.replace('".url('/')."')</script>";
        exit;
    }else{
        echo "<script>window.location.replace('".url('/register')."')</script>";
        exit;
    }
}

if(isset($_GET['logout']))
{
    $request = getRequest();
    $auth->checkUser();
    $auth->destroyUser($request['user_id']);
    echo "<script>window.location.replace('".url('/login')."')</script>";
    exit;
}
?>