<?php
if(isset($_GET['export']))
{
    $request = getRequest();
    
    $model = new Customer();
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Customer!');
    }else{
        $session->setSession('error', 'Failed export Customer!');
    }
}

if(isset($_POST['import']))
{
    $request = getRequest();
    
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Customer!');
        echo "<script>window.location.replace('".url('/customer')."')</script>";
        exit;
    }

    $model = new Customer();
    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Customer!');
    }
}

if(isset($_POST['store']))
{
    $request = getRequest();
    
    $model = new Customer();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Customer!');
    }
}

if(isset($_POST['destroy']))
{
    $request = getRequest();
    if(!isset($_POST['customer_id'])){
        $session->setSession('warning', 'Customer ID not identified!');
    }else{
        $model = new Customer();
        if($model->delete($_POST['customer_id']))
        {
            $session->setSession('success', 'Success delete Customer!');
        }else{
            $session->setSession('warning', 'Failed delete Customer!');
        }
    }
}

if(isset($_POST['update']))
{
    $request = getRequest();
    if(!isset($_POST['customer_id'])){
        $session->setSession('warning', 'Customer ID not identified!');
    }else{
        $model = new Customer();        
        $model = $model->update($_POST['customer_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Customer!');
        }else{
            $session->setSession('warning', 'Failed edit Customer!');
        }
    }
}

echo "<script>window.location.replace('".url('/customer')."')</script>";
exit;

?>