<?php
if(isset($_GET['export']))
{
    $request = getRequest();
    
    $model = new DataPelanggan();
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Data Pelanggan!');
    }else{
        $session->setSession('error', 'Failed export Data Pelanggan!');
    }
}

if(isset($_POST['import']))
{
    $request = getRequest();
    
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Data Pelanggan!');
        echo "<script>window.location.replace('".url('/data_pelanggans')."')</script>";
        exit;
    }

    $model = new DataPelanggan();
    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Data Pelanggan!');
    }
}

if(isset($_POST['centeroid']))
{
    $model_data = new DataPelanggan();
    $centeroid = $model_data->getFirstCenteroid();

    if(!empty($centeroid))
    {
        $session->setSession('success', 'Success Proses Data Pelanggan!');
        echo "<script>window.location.replace('".url('/data_pelanggans/proses_data')."')</script>";
        exit;
    }
}

if(isset($_POST['store']))
{
    $request = getRequest();
    
    $model = new DataPelanggan();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Data Pelanggan!');
    }
}

if(isset($_POST['destroy']))
{
    $request = getRequest();
    if(!isset($_POST['data_pelanggan_id'])){
        $session->setSession('warning', 'Data Pelanggan ID not identified!');
    }else{
        $model = new DataPelanggan();
        if($model->delete($_POST['data_pelanggan_id']))
        {
            $session->setSession('success', 'Success delete Data Pelanggan!');
        }else{
            $session->setSession('warning', 'Failed delete Data Pelanggan!');
        }
    }
}

if(isset($_POST['truncate']))
{
    $request = getRequest();
    
    $model = new DataPelanggan();
    $model = $model->truncate();

    if($model)
    {
        $session->setSession('success', 'Success clear all Data Pelanggan!');
    }else{
        $session->setSession('warning', 'Failed clear Data Pelanggan!');
    }
}

if(isset($_POST['update']))
{
    $request = getRequest();
    if(!isset($_POST['data_pelanggan_id'])){
        $session->setSession('warning', 'Data Pelanggan ID tidak teridentifikasi!');
    }else{
        $model = new DataPelanggan();
        
        $model = $model->update($_POST['data_pelanggan_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Data Pelanggan!');
        }else{
            $session->setSession('warning', 'Failed edit Data Pelanggan!');
        }
    }
}

// Create Partner
if(isset($_POST['partner_store']))
{
    $request = getRequest();
    
    $model = new Customer();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Customer!');
    }
    if(isset($_POST['data_pelanggan_id']))
    {
        echo "<script>window.location.replace('".url('/data_pelanggans/edit', $_POST['data_pelanggan_id'])."')</script>";
        exit;
    }
    echo "<script>window.location.replace('".url('/data_pelanggans/create')."')</script>";
    exit;
}
// Create Product
if(isset($_POST['product_store']))
{
    $request = getRequest();
    
    $model = new Product();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Product!');
    }
    if(isset($_POST['data_pelanggan_id']))
    {
        echo "<script>window.location.replace('".url('/data_pelanggans/edit', $_POST['data_pelanggan_id'])."')</script>";
        exit;
    }
    echo "<script>window.location.replace('".url('/data_pelanggans/create')."')</script>";
    exit;
}

echo "<script>window.location.replace('".url('/data_pelanggans')."')</script>";
exit;

?>