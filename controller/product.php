<?php
if(isset($_GET['export']))
{
    $request = getRequest();
    
    $model = new Product();
    $export = $model->export();
    
    if($export)
    {
        $session->setSession('success', 'Success export Product!');
    }else{
        $session->setSession('error', 'Failed export Product!');
    }
}

if(isset($_POST['import']))
{
    $request = getRequest();
    
    $file = $_FILES['file_import'];

    $upload = fileUpload($file);

    if(!$upload)
    {
        $session->setSession('error', 'Failed import Product!');
        echo "<script>window.location.replace('".url('/product')."')</script>";
        exit;
    }

    $model = new Product();
    $import = $model->import($upload['dirname'].'/'.$upload['basename'], $upload['extension']);
    
    if(!empty($import))
    {
        $session->setSession('success', 'Success import Product!');
    }
}

if(isset($_POST['store']))
{
    $request = getRequest();
    
    $model = new Product();
    $model = $model->create($request);

    if(!empty($model))
    {
        $session->setSession('success', 'Success create New Product!');
    }
}

if(isset($_POST['destroy']))
{
    $request = getRequest();
    if(!isset($_POST['product_id'])){
        $session->setSession('warning', 'Product ID not identified!');
    }else{
        $model = new Product();
        if($model->delete($_POST['product_id']))
        {
            $session->setSession('success', 'Success delete Product!');
        }else{
            $session->setSession('warning', 'Failed delete Product!');
        }
    }
}

if(isset($_POST['update']))
{
    $request = getRequest();
    if(!isset($_POST['product_id'])){
        $session->setSession('warning', 'Product ID not identified!');
    }else{
        $model = new Product();        
        $model = $model->update($_POST['product_id'], $request);

        if(!empty($model))
        {
            $session->setSession('success', 'Success edit Product!');
        }else{
            $session->setSession('warning', 'Failed edit Product!');
        }
    }
}

echo "<script>window.location.replace('".url('/product')."')</script>";
exit;

?>