<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title"><?= $tableTitle ?? 'Data Produksi' ?></span>
            <div class="actions">
                <form action="<?=url('/data_produksis/truncate')?>" class="form-truncate" onsubmit="return confirm('Are you sure to clear all data?');" method="post">
                    <input type="hidden" name="model" value=data_produksis id="model"/>
                    <button type="button" class="waves-effect waves-grey green-text btn-flat table-detail-trigger" data-table="table-produksi">Detail</button>
            <?php
                if(count($datas) > 0){
            ?>
                    <button type="submit" name="truncate" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Clear All"><i class="material-icons red-text">delete_sweep</i></button>
            <?php
                }
            ?>
                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                </form>
            </div>
        </div>
        <table class="highlight datatable" id="table-produksi">
            <thead>
                <tr>
                    <th>Kecamatan</th>
                <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <th><?=$n?></th>
                <?php
                    }
                ?>
                    <!-- <th>Rata-rata</th> -->
            <?php
                if($use_action)
                {
            ?>
                    <th class="table-column-action">Action</th>
            <?php
                }
            ?>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datas as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['kecamatan']?></td>
                <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <td><?=$row[$k]?></td>
                <?php
                    }
                ?>
                    <!-- <td><?=$row['average']?></td> -->
            <?php
                if($use_action)
                {
            ?>
                    <td class="table-column-action">
                        <form action="<?=url('/data_produksis/destroy', $row['data_produksi_id'])?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row['kecamatan'] ?>?');" method="post">
                            <input type="hidden" name="model" value=data_produksis id="model"/>
                            <input name="data_produksi_id" id="data_produksi_id" value="<?= $row['data_produksi_id'] ?? ''?>" type="hidden"/>
                            <a href="<?=url('/data_produksis/edit', $row['data_produksi_id'])?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        </form>
                    </td>
            <?php
                }
            ?>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>