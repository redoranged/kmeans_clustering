<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Perhitungan Jarak (Euclidean Distance)</span>
            <div class="actions">
                <button class="waves-effect waves-grey green-text btn-flat table-detail-trigger" data-table="<?=$table_euclidean_id?>">Detail</button>
                <a href="javascript:void(0)" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable" id="<?=$table_euclidean_id?>">
            <thead>
                <tr>
                    <th>Kecamatan</th>
                <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <th><?=$n?></th>
                <?php
                    }
                ?>
                    <th>C1</th>
                    <th>C2</th>
                    <th>C3</th>
                    <th>Cluster</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($euc as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['kecamatan']?></td>
                <?php
                    foreach($sayur as $k => $n)
                    {
                ?>
                    <td><?=$row[$k]?></td>
                <?php
                    }
                ?>
                    <td><?=$row['C1']?></td>
                    <td><?=$row['C2']?></td>
                    <td><?=$row['C3']?></td>
                    <td><?=$row['cluster']?></td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>