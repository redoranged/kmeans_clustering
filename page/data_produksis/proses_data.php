<?php
    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Produksi',
            'link' => url('/data_produksis')
        ],
        [
            'title' => 'Proses Data',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

?>
<br>
<div class="card">
    <div class="card-content">
        <!-- Start Data Awal -->
    <?php
        $centeroidProduksi = new CenteroidProduksi();
        $model_data = new DataProduksi();
        $datas = $model_data->select();

        if(count($datas) > 0)
        {
            $use_action = false;
            $tableTitle = 'Data Awal';
            include 'view/table.php';
    ?>   
        <!-- End Data Awal -->

        <!-- Start Proses Data -->
            <div class="center-align">
                <form action="<?=url('/data_produksis')?>" method="post">
                    <input type="hidden" name="model" value="data_produksis" id="model"/>
                    <button type="submit" class="btn orange" name="centeroid">PROSES <?=(count($centeroidProduksi->select()) > 0) ? 'ULANG' : 'DATA'?></button>
                </form>
            </div>
        <!-- End Proses Data -->

        <!-- Start Iterasi  -->
    <?php
            if(count($centeroidProduksi->select()) > 0)
            {
                $centeroid = $centeroidProduksi->getCenteroid();
                $last_cluster = [];
                $loop = true;
                $iteration = 1;
                while($loop)
                {
                    $euc = $model_data->getEuclidian($centeroid);
                    $centeroid = $model_data->getNewCenteroid($euc);
                    
                    $new_cluster = [];
                    foreach ($euc as $row) {
                        $new_cluster[] = $row['cluster'];
                    }
                    if($last_cluster === $new_cluster)
                    {
                        $loop = false;
                    }else{
                        $last_cluster = $new_cluster;
                        $iteration++;
                    }
                    
                    $euc = $model_data->getEuclidian($centeroid);
                }
                $c1 = [];
                $c2 = [];
                $c3 = [];
    ?>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Hasil Clustering</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>Kecamatan</th>
                                    <th>Cluster</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($euc as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['kecamatan']?></td>
                                    <td><?=$row['cluster']?></td>
                                </tr>
                        <?php
                                if($row['cluster'] === 'C1')
                                {
                                    $c1[] = $row;
                                }
                                if($row['cluster'] === 'C2')
                                {
                                    $c2[] = $row;
                                }
                                if($row['cluster'] === 'C3')
                                {
                                    $c3[] = $row;
                                }
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- End Iterasi  -->

            <!-- Start Kesimpulan -->

            <div class="card z-depth-3">
                <div class="card-content">
                    <span class="card-title"><strong>Kesimpulan:</strong></span>
                    <p>Berdasarkan perhitungan menggunakan K-Means Clustering maka didapatkan hasil bahwa:</p>
                    <ul>
                        <li>Anggota C1 = <?=implode(', ', array_column($c1, 'kecamatan'))?></li>
                        <li>Anggota C2 = <?=implode(', ', array_column($c2, 'kecamatan'))?></li>
                        <li>Anggota C3 = <?=implode(', ', array_column($c3, 'kecamatan'))?></li>
                    </ul>
                    <br>
                    <hr>
                    <strong>Keterangan:</strong>
                    <ul>
                        <li>C1 = Produktivitas Tinggi</li>
                        <li>C2 = Produktivitas Sedang</li>
                        <li>C3 = Produktivitas Rendah</li>
                    </ul>
                </div>
            </div>
            <!-- End Kesimpulan -->
    <?php
            }
        }else{
            if(!count($centeroidProduksi->select()) > 0 && !count($datas) > 0)
            {
                echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
            }
        }
    ?>
    </div>
</div>