<?php

    $data = new DataProduksi();
    $data = $data->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Produksi',
            'link' => url('/data_produksis')
        ],
        [
            'title' => $data['kecamatan'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>