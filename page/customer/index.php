<?php
    $model_data = new Customer();

    $datas = $model_data->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Customers',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/customer/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-customer'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/customer/create')
        ]
    ];
    include_once load_component('floating-button');
?>
<br>
<?php
    include 'view/table.php';
    
    $modal = [
        'id' => 'modal-customer',
        'model' => 'customer',
        'text' => 'Excel Template : <a href="'.public_url('template/customer-template.xlsx').'" download/>customer-template.xlsx</a>',
        'action' => url('/customer/import'),
    ];

    include_once load_component('modal-import');
?>