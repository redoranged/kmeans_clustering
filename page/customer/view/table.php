<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Customer</span>
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datas as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['name']?></td>
                    <td>
                        <form action="<?=url('/customer/destroy', $row['customer_id'])?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row['name'] ?>?');" method="post">
                            <input type="hidden" name="model" value=customer id="model"/>
                            <input name="customer_id" id="customer_id" value="<?= $row['customer_id'] ?? ''?>" type="hidden"/>
                            <a href="<?=url('/customer/edit', $row['customer_id'])?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <!-- <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button> -->
                        </form>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>