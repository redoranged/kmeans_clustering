<form class="card z-depth-3" action="<?=(($data ?? '') != '') ? url('/customer/update', $data['customer_id']) : url('/customer/store')?>" method="post">
    <input type="hidden" name="model" value="customer" id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="customer_id" id="customer_id" value="<?= $data['customer_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="customer_id">Customer ID</label>
            </div>
            <div class="input-field">
                <input name="name" id="name" value="<?= $data['name'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="name">Name</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($data ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($data ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>