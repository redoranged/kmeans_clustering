<?php

    $data = new Customer();
    $data = $data->find($_GET['id']);

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Customers',
            'link' => url('/customer')
        ],
        [
            'title' => $data['name'],
            'link' => 'javascript:void(0)'
        ],
    ];

    include_once load_component('breadcrumb');


?>
<br>
<?php
    include 'view/form.php';
    
?>