<form class="card z-depth-3" action="<?=(($data ?? '') != '') ? url('/product/update', $data['product_id']) : url('/product/store')?>" method="post">
    <input type="hidden" name="model" value="product" id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="product_id" id="product_id" value="<?= $data['product_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="product_id">Product ID</label>
            </div>
            <div class="input-field">
                <input name="name" id="name" value="<?= $data['name'] ?? ''?>" type="text" class="validate" required autofocus>
                <label for="name">Name</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($data ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($data ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>