<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Perhitungan Jarak (Euclidean Distance)</span>
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Pelanggan</th>
                    <th>C1</th>
                    <th>C2</th>
                    <th>C3</th>
                    <th>C4</th>
                    <th>Cluster</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($euc as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['pelanggan']['name']?></td>
                    <td><?=$row['C1']?></td>
                    <td><?=$row['C2']?></td>
                    <td><?=$row['C3']?></td>
                    <td><?=$row['C4']?></td>
                    <td><?=$row['cluster']?></td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>