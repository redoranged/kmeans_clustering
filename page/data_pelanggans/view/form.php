<?php
  $model_customer = new Customer();
  $customer_ids = $model_customer->select();

  $model_product = new Product();
  $product_ids = $model_product->select();
?>

<!-- Modal Form -->
    <!-- Partner -->
    <form id="modal-partner" action="<?=url('/data_pelanggans/partner/store')?>" method="post" class="modal">
        <input type="hidden" name="model" value="data_pelanggans" id="model"/>
<?php
    if(($data ?? '') != '')
    {
?>
        <input type="hidden" name="data_pelanggan_id" value="<?=$data['data_pelanggan_id']?>"/>
<?php
    }
?>
        <div class="modal-content">
            <h5>New Customer</h5>
            <div class="input-field">
                <input name="name" id="customer_name" type="text" class="validate" required>
                <label for="customer_name">Customer Name</label>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" name="partner_store" class="waves-effect waves-grey green-text btn-flat">Create</a>
            <button type="button" class="modal-close waves-effect waves-grey red-text btn-flat">Close</a>
        </div>
    </form>

    <!-- Product -->
    <form id="modal-product" action="<?=url('/data_pelanggans/product/store')?>" method="post" class="modal">
        <input type="hidden" name="model" value="data_pelanggans" id="model"/>
<?php
    if(($data ?? '') != '')
    {
?>
        <input type="hidden" name="data_pelanggan_id" value="<?=$data['data_pelanggan_id']?>"/>
<?php
    }
?>
        <div class="modal-content">
            <h5>New Product</h5>
            <div class="input-field">
                <input name="name" id="product_name" type="text" class="validate" required>
                <label for="product_name">Product Name</label>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" name="product_store" class="waves-effect waves-grey green-text btn-flat">Create</a>
            <button type="button" class="modal-close waves-effect waves-grey red-text btn-flat">Close</a>
        </div>
    </form>
    
<!-- Main Form -->
<form class="card z-depth-3" action="<?=(($data ?? '') != '') ? url('/data_pelanggans/update', $data['data_pelanggan_id']) : url('/data_pelanggans/store')?>" method="post">
    <input type="hidden" name="model" value="data_pelanggans" id="model"/>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="data_pelanggan_id" id="data_pelanggan_id" value="<?= $data['data_pelanggan_id'] ?? ''?>" type="text" class="validate" readonly>
                <label for="data_pelanggan_id">Data ID</label>
            </div>
            <div class="input-field row">
                <div class="col s11">
                    <select name="customer_id" id="customer_id" class="select2 validate" required>
                        <option value="" disabled selected>Choose your customer</option>
                <?php
                    foreach ($customer_ids as $key => $customer) 
                    {
                ?>
                        <option value="<?=$customer['customer_id']?>" <?=(($data['customer_id'] ?? '') == $customer['customer_id']) ? 'selected' : ''?>><?=$customer['name']?></option>
                <?php
                    }
                ?>
                    </select>
                </div>
                <button type="button" class="col s1 waves-effect waves-light green lighten-1 btn-small modal-trigger tooltipped" data-position="right" data-tooltip="Create new Customer" data-target="modal-partner"><i class="material-icons">add</i></a>
            </div>
            <div class="input-field">
                <input name="invoice_no" id="invoice_no" value="<?= $data['invoice_no'] ?? '0'?>" type="text" class="validate">
                <label for="invoice_no">No Invoice</label>
            </div>
            <div class="input-field">
                <input name="tanggal_transaksi" id="tanggal_transaksi" value="<?= $data['tanggal_transaksi'] ?? ''?>" type="date" class="validate">
                <label for="tanggal_transaksi">Tanggal Transaksi</label>
            </div>
            <div class="input-field row">
                <div class="col s11">
                    <select name="product_id" id="product_id" class="select2 validate" required>
                        <option value="" disabled selected>Choose your product</option>
                <?php
                    foreach ($product_ids as $key => $product) 
                    {
                ?>
                        <option value="<?=$product['product_id']?>" <?=(($data['product_id'] ?? '') == $product['product_id']) ? 'selected' : ''?>><?=$product['name']?></option>
                <?php
                    }
                ?>
                    </select>
                </div>
                <button type="button" class="col s1 waves-effect waves-light green lighten-1 btn-small modal-trigger tooltipped" data-position="right" data-tooltip="Create new Product" data-target="modal-product"><i class="material-icons">add</i></a>
            </div>
            <div class="input-field">
                <input name="jumlah" id="jumlah" value="<?= $data['jumlah'] ?? '0'?>" type="number" class="validate floatTextBox">
                <label for="jumlah">Jumlah Produk</label>
            </div>
            <div class="input-field">
                <input name="harga_satuan" id="harga_satuan" value="<?= $data['harga_satuan'] ?? '0'?>" type="text" class="validate floatTextBox">
                <label for="harga_satuan">Harga Satuan</label>
            </div>
            <div class="input-field">
                <input name="total_bayar" id="total_bayar" value="<?= $data['total_bayar'] ?? '0'?>" type="text" class="validate floatTextBox">
                <label for="total_bayar">Total Bayar</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($data ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($data ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>

<script>
// Calculate Total Bayar
$(document).ready(function(){
    function calculateTotalBayar()
    {
        jumlah = parseFloat($('#jumlah').val())
        harga_satuan = parseFloat($('#harga_satuan').val())

        $('#total_bayar').val(jumlah * harga_satuan)
    }

    $('#jumlah').on('change keyup focusout', function(){
        calculateTotalBayar()
    })
    $('#harga_satuan').on('change keyup focusout', function(){
        calculateTotalBayar()
    })
})


// Restricts input for the given textbox to the given inputFilter function.
function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}

var floatTextBox = document.getElementsByClassName("floatTextBox")
for (var i = 0; i < floatTextBox.length; i++) {
    setInputFilter(floatTextBox[i], function(value) {
      return /^-?\d*[.]?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
    });
}
// $(".floatTextBox").inputFilter(function(value) {
//   return /^-?\d*[.]?\d*$/.test(value); });
</script>