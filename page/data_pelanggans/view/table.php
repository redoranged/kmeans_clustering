<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title"><?= $tableTitle ?? 'Data Pelanggan' ?></span>
            <div class="actions">
                <form action="<?=url('/data_pelanggans/truncate')?>" class="form-truncate" onsubmit="return confirm('Are you sure to clear all data?');" method="post">
                    <input type="hidden" name="model" value=data_pelanggans id="model"/>
            <?php
                if(count($datas) > 0){
            ?>
                    <button type="submit" name="truncate" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Clear All"><i class="material-icons red-text">delete_sweep</i></button>
            <?php
                }
            ?>
                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                </form>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Pelanggan</th>
                    <th>No Invoice</th>
                    <th>Tanggal Transaksi</th>
                    <th>Produk</th>
                    <th>Jumlah Produk</th>
                    <th>Harga Satuan</th>
                    <th>Total Bayar</th>
            <?php
                if($use_action)
                {
            ?>
                    <th>Action</th>
            <?php
                }
            ?>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datas as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row['customer']['name']?></td>
                    <td><?=$row['invoice_no']?></td>
                    <td><?=$row['tanggal_transaksi']?></td>
                    <td><?=$row['product']['name']?></td>
                    <td><?=$row['jumlah']?></td>
                    <td><?=$row['harga_satuan']?></td>
                    <td><?=$row['total_bayar']?></td>
            <?php
                if($use_action)
                {
            ?>
                    <td>
                        <form action="<?=url('/data_pelanggans/destroy', $row['data_pelanggan_id'])?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row['customer']['name'].' ('.$row['product']['name'].') / '.date('d F Y', strtotime($row['tanggal_transaksi']))?>?');" method="post">
                            <input type="hidden" name="model" value=data_pelanggans id="model"/>
                            <input name="data_pelanggan_id" id="data_pelanggan_id" value="<?= $row['data_pelanggan_id'] ?? ''?>" type="hidden"/>
                            <a href="<?=url('/data_pelanggans/edit', $row['data_pelanggan_id'])?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        </form>
                    </td>
            <?php
                }
            ?>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>