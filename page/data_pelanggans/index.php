<?php
    $model_data = new DataPelanggan();

    $datas = $model_data->select();

    // Breadcrumb setup
    $breadcrumb_items = [
        [
            'title' => 'Home',
            'link' => url('/')
        ],
        [
            'title' => 'Data Pelanggan',
            'link' => 'javascript:void(0)'
        ]
    ];

    include_once load_component('breadcrumb');

    // Floating Button Setup
    $button_items = [
        [
            'name' => 'Export Data',
            'icon' => 'file_download',
            'class' => 'purple',
            'link' => url('/data_pelanggans/export')
        ],
        [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-data_pelanggans'
        ],
        [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => url('/data_pelanggans/create')
        ]
    ];

    if(count($datas) > 0)
    {
        array_unshift($button_items, [
            'name' => 'Proses Data',
            'icon' => 'cached',
            'class' => 'orange',
            'link' => url('/data_pelanggans/proses_data')
        ]);
    }

    include_once load_component('floating-button');
?>
<br>
<?php
    $use_action = true;
    include 'view/table.php';

    $modal = [
        'id' => 'modal-data_pelanggans',
        'name' => 'Data Pelanggan',
        'model' => 'data_pelanggans',
        'text' => 'Excel Template : <a href="'.public_url('template/pelanggan-template.xlsx').'" download/>pelanggan-template.xlsx</a> atau <a href="'.public_url('template/sample data pelanggan.xlsx').'" download/>sample data pelanggan.xlsx</a>',
        'action' => url('/data_pelanggans/import'),
    ];

    include_once load_component('modal-import');
?>