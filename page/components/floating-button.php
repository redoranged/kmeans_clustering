<div class="fixed-action-btn">
    <a class="btn-floating btn-large red">
        <i class="large material-icons">menu</i>
    </a>
    <ul>
        <?php
        foreach ($button_items as $key => $button_item) {
        ?>
            <li><a href="<?=$button_item['link']?>" class="btn-floating <?=$button_item['class']?> tooltipped" data-position="left" data-tooltip="<?=$button_item['name']?>"><i class="material-icons"><?=$button_item['icon']?></i></a></li>
        <?php
        }
        ?>
    </ul>
</div>